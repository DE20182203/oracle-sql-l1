Topic 1: 
Data Retrieval manipulation
Assignment : 1

a. select count(EMPNO) "Number of Employees", avg(SAL) "Average Salary" from Employee where DEPTNO = 20;
b. select ENAME, SAL, SAL * 0.1 PF from Employee;
c. select ENAME, JOB from Employee where MGR is null;
d. select ENAME, EMPNO, JOB, HIREDATE from Employee where EMPNO in (select MGR from Employee);
e. select EMPNO, ENAME, SAL from Employee where MGR = 7369;
f. select * from Employee where JOB = 'Clerk' or JOB = 'Trainer';
g. select * from Employee where SAL between 10000 and 20000;
h. select ENAME from Employee where COMM is null;
i. select * from Employee where ENAME like 'H%' or ENAME like '%H';
j. select * from Employee order by SAL;
k. select * from Employee where DEPTNO not in (10, 20, 30);
l. select EMPNO, ENAME, SAL * 12 "AnnualSalary" from Employee where DEPTNO = 10;

---------------------------------------------------------------------------------------------------------------------
Assignment : 2

a. select * from Books where Author_name = 'Loni' and Cost < 600;
b. select * from Issue where Return_date is null;
c. select * from Issue where (Return_date - Issue_Date) > 30;
d. select * from Books where Category = 'Database' and Cost between 500 and 750;
e. select * from Books where Category in ('Science', 'Database', 'Fiction', 'Management');
f. select * from Member order by Penalty_Amount desc;
g. select * from Books order by Category, Cost desc;
h. select * from Books where Book_Name like '%SQL%';
i. select * from Member where Member_Name like '%I%' and (Member_Name like 'R%' or Member_Name like 'G%');
j. select initcap(Book_Name), Author_name from Books order by Book_Name desc;
k. select to_char(Issue_Date, 'Day MON, DD, YY'), to_char(Return_date, 'Day MON, DD, YY') from Issue where Member_Id = 101;
l. select * from Member where Acc_Open_Date > '31-DEC-00';

-------------------------------------------------------------------------------------------------------------------------------
Topic 2: 
Data Types
Assignment : 1

a. NUMERIC is an invalid data type, while NUMBER (10, 2) is a valid number data type with precision and scale in Oracle SQL.
b. CHAR(48) is fixed-length character data type, whereas VARCHAR(48) is a dynamic-length character data type in Oracle SQL.
c. CHAR is fixed-length character data type, VARCHAR2 is dynamic-length character data type, CLOB is a large character data type containing single-byte or multibyte characters.
d. VARCHAR2, NVARCHAR2, NUMBER, FLOAT, LONG, DATE, BINARY_FLOAT, BINARY_DOUBLE, TIMESTAMP, TIMESTAMP WITH TIME ZONE, TIMESTAMP WITH LOCAL TIME ZONE, INTERVAL YEAR TO MONTH, INTERVAL DAY TO SECOND, RAW, LONG RAW, ROWID, UROWID, CHAR, NCHAR, CLOB, NCLOB, BLOB, BFILE.
e. CLOB contains a character large object containing single-byte or multibyte characters. BLOB contains a binary large object.
f. DATE 
g. DD-MON-YY
h. select to_char(sysdate, 'MON DD YYYY') from dual;
i. BINARY_FLOAT is 32-bit floating point number of 4 bytes, whereas NUMBER is a numeric value which has having precision p and scale s, ranges between 1 to 22 bytes.
j. BFILE Contains a locator to a large binary file stored outside the database. It enables byte stream I/O access to external LOBs residing on the database server.

---------------------------------------------------------------------------------------------------------------------------
Topic 3: 
Aggregation and Sub-Queries
Assignment : 1

a. select count(*) "Number of Employees" from Employee;
b. select count(JOB) "Number of Designations" from Employee;
c. select SUM(SAL) "Total Salary" from Employee;
d. select MAX(SAL) "Maximum Salary", MIN(SAL) "Minimum Salary", AVG(SAL) "Average Salary" from Employee;
e. select MAX(SAL) "Maximum Salary for Salesman" from Employee where JOB = 'Salesman';
f. select * from Vehicle v join Shipment s on (v.VehNo = s.VehicleNo) join Customer c on (s.CustId = c.CustId) where c.CustIncome > 120000;
g. select c.CustName, c.CustIncome from Customer c join Shipment s on (s.CustId = c.CustId) where s.VehicleNo = (select VehNo from Vehicle where VehDriver = 'Baskar');
h. select * from Customer where (select count(distinct(VehicleNo)) from Shipment where Shipment.CustId = Customer.CustId) >= (select count(*) from Vehicle);
i. select * from Customer where CustIncome > 500000 and CustId = (select CustId from Shipment where ShipWeight > 100 or ShipDestination = 'NewDelhi');
j. select * from Vehicle where VehNo = (select MAX(VehicleNo) from Shipment);
------------------------------------------------------------------------------------------------------------------------------
Assignment : 2

a. select count(*) "Number of Members" from Member;
b. select count(*) "Number of Books Issued" from Issue;
c. select avg(Fees_paid) "Average Membership Fees" from Member;
d. select Book_No from Books where Book_No not in (select Book_No from Issue);
e. select Member_Id from Member where Member_Id in (select Member_Id from Issue);
f. select Member_Id "Highest Books" from Member where Member_Id = (select MAX(Member_Id) from Issue);
   select Member_Id "Lowest Books" from Member where Member_Id = (select MIN(Member_Id) from Issue);
g. select Book_No, Book_Name from Books where Book_No in (select Book_No from Issue where to_char(Issue_Date, 'Month') in ('June', 'December'));
h. select b.Book_No, b.Book_Name, i.Issue_Date, i.Return_date from Books b join Issue i using (Book_No) where i.Member_Id = (select Member_Id from Member where Member_Name = 'Stalin');
i. select * from Books where cost in (select MAX(Cost) from Books group by Category);
j. select m.Member_Id, m.Member_Name from Member m, Issue i where m.Max_Books_Allowed < (select(count(Book_No) from Issue where m.Member_Id = i.Member_Id) and m.Member_Id = i.Member_Id group by m.Member_name, m.Member_Id;
--------------------------------------------------------------------------------------------------------------------

Topic 4: 
Creating and Managing Database objects
Assignment : 1

a. create table empcopy as (select * from Employee);
b. create table deptcopy as (select * from Department where 1=0);
c. create index book_name_index on Books(Book_Name);
d. create index d_index on Issue(Member_Id, Book_No);
e. alter table Member modify Member_Name varchar(48);
f. alter table Issue add Reference char(30);
g. alter table Issue rename to Lib_Issue;
h. alter table Issue drop column Reference;
i. alter table Member add constraint CHK_MEMBER check (Max_Books_Allowed < 100 and Penalty_Amount <= 1000);
j. create sequence MEMBER_SEQ increment by 1 start with 1 minvalue 0 nocycle nocache;
   create sequence SUPPLIER_SEQ increment by 1 start with 1 minvalue 0 nocycle nocache;
   insert into Member (Member_Id) values (MEMBER_SEQ.nextval);
   insert into Supplier (SupplierId) values (SUPPLIER_SEQ.nextval);
k. create sequence no_seq increment by 2 start with 100 maxvalue 200 nocycle nocache;
l. drop sequence no_seq;
m. create sequence book_seq increment by 1 start with 101 maxvalue 1000 nocycle;
n. create sequence member_seq increment by 1 start with 1 maxvalue 100 nocycle;
o. drop sequence book_seq;
   drop sequence member_seq;
   
----------------------------------------------------------------------------------------------------------------------------------
Topic 5: 
Data Manipulation
Assignment : 1

a. insert into Employee values (100,'A','PEON',546,'09-JAN-19',2000,NULL,16);
   insert into Employee values (101,'B','PEON',546,'09-JAN-19',2000,NULL,16);
   insert into Employee values (102,'C','PEON',546,'09-JAN-19',2000,NULL,16);
   insert into Employee values (103,'D','PEON',546,'09-JAN-19',2000,5,16);
   insert into Employee values (104,'E','PEON',546,'09-JAN-19',3000,NULL,16);
   insert into Employee values (105,'F','PEON',546,'09-JAN-19',2000,NULL,16);
   insert into Employee values (106,'G','PEON',579,'09-JAN-19',2000,NULL,16);
   insert into Employee values (107,'H','PEON',579,'09-JAN-19',3000,NULL,16);
   insert into Employee values (108,'Z','PEON',549,'09-JAN-19',2000,10,16);
   insert into Employee values (109,'Y','PEON',549,'09-JAN-19',2000,NULL,16);
   insert into Employee values (110,'X','PEON',549,'09-JAN-19',2000,NULL,16);
   insert into Employee values (120,'W','PEON',579,'09-JAN-19',5000,NULL,16);
   insert into Employee values (130,'V','PEON',200,'09-JAN-19',2000,NULL,16);
   insert into Employee values (140,'U','PEON',545,'09-JAN-19',2000,NULL,16);
   insert into Employee values (150,'T','PEON',545,'09-JAN-19',7000,10,16);
b. update Employee set SAL = SAL * 1.1 where DEPTNO in (10, 20);
c. update Employee set SAL = SAL * 1.1 and COMM = COMM * 1.02 where COMM is not null;
d. update Employee set JOB = 'MANAGER' where EMPNO in (select MGR from Employee);
e. delete from Employee where to_char(HIREDATE, 'YEAR') < 1980;
f. update Member set Max_Books_Allowed = 110 where Member_Id = 100;
   Error -> ORA-02290: check constraint CHK_MEMBER violated
   Reason -> Check constraint was set to Max_Books_Allowed column earlier where its value was set as must be greater than 100, hence the new value of 110 is violating the constraint.
g. insert into NewBook select * from Books;
h. COMMIT;
i. insert into Books values (1000, 'MongoDB', 'Allen', 750, 'Database');
   insert into Books values (1001, 'Python 3', 'Magret', 1350, 'AI');
   insert into Books values (1002, 'Angular 4', 'Google', 2750, 'UI');
   insert into Books values (1003, 'PL SQL-Ref', 'Scott Urman', 750, 'Database');
j. create type t_emp as object(EMPNO number, ENAME varchar2(40), JOB varchar2(20), MGR number, HIREDATE date, SAL number, COMM number, DEPTNO number);
   create type t_emplist as table of t_emp;
   insert into Employee values (5, t_emplist(t_emp(160,'I','WAITER',545,'18-MAR-95',5000,7,35),
											 t_emp(170,'J','WAITER',545,'18-MAR-95',5000,7,35),
											 t_emp(180,'K','WAITER',590,'18-MAR-95',8000,7,35),
											 t_emp(190,'L','WAITER',545,'18-MAR-95',5000,7,35),
											 t_emp(260,'M','WAITER',590,'18-MAR-95',2000,NULL));
k. ROLLBACK;
l. update Books set Cost = 300 and Category = 'OracleDatabase' where Book_No = 1003;
m. SAVEPOINT T;
   delete from Issue where Member_Id = 1 and Issue_Date < '10-DEC-06';
   delete from Books where Category not in (RDBMS', 'Database');
   ROLLBACK to T;
n. COMMIT;

----------------------------------------------------------------------------------------------------------------------
Topic 6: 
HQ and DML, DDL Extension
Assignment 1:

select ENAME, EMPNO, MGR, LEVEL from Employee 
		start with EMPNO = (select EMPNO from Employee where MGR is null) 
			connect by prior EMPNO = MGR 
				order siblings by ENAME;
				
------------------------------------------------------------------------------------------------
Assignment 2:

create table emp_load (EMPNO number, ENAME varchar2(40), HIREDATE date) 
		organization external 
			(type ORACLE_LOADER,
			default directory DEF_DIR,
			access parameters(RECORDS DELIMITED BY NEWLINE FIELDS
				(EMPNO number, ENAME varchar2(40), HIREDATE date) 
			)
		)
	location('info.csv')
);
---------------------------------------------------------------------------------------------------------------
Topic 7: 
Advanced SQL
Assignment 1:

Order in which the steps get executed:-

SQL> EXPLAIN PLAN FOR 
select e.employee_id,e.first_name,d.department_name
from hr.employees
E, hr.departments D
where e.department_id = d.department_id;

SQL> SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY());

Output>

PLAN_TABLE_OUTPUT
Plan hash value: 2052257371

----------------------------------------------------------------------------------
| Id | Operation | Name | Rows | Bytes | Cost (%CPU)| Time |
----------------------------------------------------------------------------------
| 0 | SELECT STATEMENT | | 106 | 3180 | 7 (15)| 00:00:01 |
|* 1 | HASH JOIN | | 106 | 3180 | 7 (15)| 00:00:01 |
| 2 | TABLE ACCESS FULL| DEPARTMENTS | 30 | 480 | 3 (0)| 00:00:01 |
| 3 | TABLE ACCESS FULL| EMPLOYEES | 107 | 1498 | 3 (0)| 00:00:01 |
----------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

1 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")

SQL> alter session set optimizer_mode=first_rows;

Output>

PLAN_TABLE_OUTPUT
Plan hash value: 2912831499

-------------------------------------------------------------------------------------------------
| Id | Operation | Name | Rows | Bytes | Cost (%CPU)| Time |
-------------------------------------------------------------------------------------------------
| 0 | SELECT STATEMENT | | 106 | 3180 | 12 (0)| 00:00:01 |
| 1 | TABLE ACCESS BY INDEX ROWID| EMPLOYEES | 4 | 56 | 1 (0)| 00:00:01 |
| 2 | NESTED LOOPS | | 106 | 3180 | 12 (0)| 00:00:01 |
| 3 | TABLE ACCESS FULL | DEPARTMENTS | 30 | 480 | 3 (0)| 00:00:01 |
|* 4 | INDEX RANGE SCAN | EMP_DEPARTMENT_IX | 10 | | 0 (0)| 00:00:01 |
-------------------------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

4 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")

Reason behind the modified output> With the default optimizer_mode of all_rows, the query performs two full-scan operations against the target tables and feeds these into a hash join.  This is consistent with the optimizer goal of all_rows, which is to minimize computing resources. When we change the optimizer_mode to first_rows, we see that the execution plan changes, and the hash join is replaced by a nested loops join operation, and the full table scans are replaced by index range scans.  The first_rows access incurs additional I/O (more than the full scans in the all_rows plan), but the index access ensures the fastest possible response time.

-------------------------------------------------------------------------------------------------------------
Assignment 2:

SQL> ALTER SESSION SET sql_trace=TRUE;

SQL> select * from employee where emp_id = 200

SQL> ALTER SESSION SET SQL_TRACE = FALSE;

SQL> exec start_trace(17, 6157, 30);

Tracing Started for User: hr
Tracing Start Time: 01-13-2019 20:55:12
Tracing Stop Time: 01-13-2019 20:55:42
Trace Directory: C:\Oracle\admin\ORCL92\udump
Trace Filename: ORCL92_ora_5472.trc

tkprof Output>

tkprof: Release 9.2.0.1.0 - Production on Tue Dec 24 15:32:43 2002

Copyright (c) 1982, 2002, Oracle Corporation. All rights reserved.

Trace file: ORCL92_ora_5472.trc
Sort options: default

********************************************************************************

count = number of times OCI procedure was executed
cpu = cpu time in seconds executing
elapsed = elapsed time in seconds executing
disk = number of physical reads of buffers from disk
query = number of buffers gotten for consistent read
current = number of buffers gotten in current mode (usually for update)
rows = number of rows processed by the fetch or execute call
********************************************************************************


select *
from
employee where emp_id = 200


call count cpu elapsed disk query current rows

------- ------ -------- ---------- ---------- ---------- ---------- ----------

Parse 10 0.00 0.03 0 0 0 0

Execute 10 0.00 0.00 0 0 0 0

Fetch 20 0.34 0.35 72 4730 0 10

------- ------ -------- ---------- ---------- ---------- ---------- ----------

total 40 0.34 0.39 72 4730 0 10


Misses in library cache during parse: 1
Optimizer goal: CHOOSE
Parsing user id: 59

Rows Row Source Operation
------- ---------------------------------------------------
1 TABLE ACCESS FULL EMPLOYEE

********************************************************************************

.............................................................................................................

Assignment 4:

Before Indexing>

SQL> EXPLAIN PLAN FOR select * from EMPLOYEE where MGR IS NULL;
SQL> SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY());

Output>

PLAN_TABLE_OUTPUT
Plan hash value: 1896031711

--------------------------------------------------------------------------
| Id | Operation | Name | Rows | Bytes | Cost (%CPU)| Time |
--------------------------------------------------------------------------
| 0 | SELECT STATEMENT | | 9 | 1197 | 3 (0)| 00:00:01 |
|* 1 | TABLE ACCESS FULL| EMPLOYEE | 9 | 1197 | 3 (0)| 00:00:01 |
--------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

1 - filter("MGR" IS NULL)

-----

After Indexing>

SQL> create index MGR_IDX ON Employee(upper(MGR));

SQL> EXPLAIN PLAN FOR select /*+ INDEX_RS_ASC(e MGR_IDX) */ * from EMPLOYEE e where MGR IS NULL;
SQL> SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY());

Output>

PLAN_TABLE_OUTPUT
Plan hash value: 4031004625

---------------------------------------------------------------------------------------
| Id | Operation | Name | Rows | Bytes | Cost (%CPU)| Time |
---------------------------------------------------------------------------------------
| 0 | SELECT STATEMENT | | 9 | 1197 | 2 (0)| 00:00:01 |
| 1 | TABLE ACCESS BY INDEX ROWID| EMPLOYEE | 9 | 1197 | 2 (0)| 00:00:01 |
|* 2 | INDEX RANGE SCAN | MGR_IDX | 9 | | 1 (0)| 00:00:01 |
---------------------------------------------------------------------------------------

Predicate Information (identified by operation id):
---------------------------------------------------

1 - filter("MGR" IS NULL)

-----
